import json 


class Player:
    def __init__(self, name, guid):
        self.name = name
        self.guid = guid
        self.wins = 0
        self.score = 0
        self.games = []

    def shortname(self):
        return self.name.split()[0] + ' ' + self.name.split()[1][0] + '.'

    def __lt__(self, other):
        return Player.cmp(self, other) < 0

    def __gt__(self, other):
        return Player.cmp(self, other) > 0

    def __eq__(self, other):
        return self.guid == other.guid or Player.cmp(self, other) == 0

    def __le__(self, other):
        return Player.cmp(self, other) <= 0  

    def __ge__(self, other):
        return Player.cmp(self, other) >= 0

    def __ne__(self, other):
        return Player.cmp(self, other) != 0

    @classmethod
    def cmp(cls, a, b):
        if a.wins > b.wins:
            return -1
        elif b.wins > a.wins:
            return 1
        elif a.score > b.score:
            return -1
        elif b.score > a.score:
            return 1

        match = [g for g in a.games if g.player_1 == b or g.player_2 == b]
        if len(match) == 0:
            if a.name > b.name:
                return -1
            else:
                return 1
        match = match[0]

        if match.score_1 > match.score_2:
            return -1 if match.player_1 == a else 1
        return 1 if match.player_1 == a else -1


class Game:
    def __init__(self, player_1, player_2, score_1, score_2):
        self.player_1 = player_1
        self.player_2 = player_2
        self.score_1 = score_1
        self.score_2 = score_2


class LeaderBoard:
    def __init__(self, file_name):
        with open(file_name) as score_data:
            self.source = json.load(score_data)

        if "houses" not in self.source:
            return

        # Extract houses
        self.houses = {house_name: [Player(name, guid) for guid, name in house.items()] for house_name, house in self.source["houses"].items()}

        if "games" not in self.source:
            return
        # Extract games and calculate scores.
        self.games = []
        for game in self.source["games"]:
            player_1 = self.find_player(game["player_1"])
            player_2 = self.find_player(game["player_2"])

            game = Game(player_1, player_2, game["score_1"], game["score_2"])
            self.games.append(game)
            player_1.games.append(game)
            player_2.games.append(game)

            player_1.score += game.score_1 - game.score_2
            player_2.score += game.score_2 - game.score_1

            if player_1.score > player_2.score:
                player_1.wins += 1
            else:
                player_2.wins += 1

        self.bracket_players = []
        self.bracket_games = []
        
        if "brackets" not in self.source:
            return

        self.first_bracket_round = []
        self.second_bracket_round = []
        self.final_bracket_round = []
        self.bracket_players = [self.find_player(i) for i in self.source["brackets"]]

        for game in self.source["bracket_games"]:
            print(game)
            player_1 = self.find_player(game["player_1"])
            player_2 = self.find_player(game["player_2"])
            self.bracket_games.append(Game(player_1, player_2, game["wins_1"], game["wins_2"]))

        winners = []
        for a, b in chunks(self.bracket_players, 2):
            game = self.find_bracket_game(a, b)
            if game is None:
                self.first_bracket_round.append([])
                continue

            self.first_bracket_round.append([game.score_1, game.score_2])
            if game.score_1 > game.score_2:
                winners.append(a)
            else:
                winners.append(b)

        final_match = []
        third_place = []
        for a, b in chunks(winners, 2):
            game = self.find_bracket_game(a, b)
            if game is None:
                self.second_bracket_round.append([])
                continue

            self.second_bracket_round.append([game.score_1, game.score_2])
            if game.score_1 > game.score_2:
                final_match.append(a)
                third_place.append(b)
            else:
                final_match.append(b)
                third_place.append(a)

        self.final_bracket_round = [[], []]

        if len(final_match) > 0:
            final_match = self.find_bracket_game(final_match[0], final_match[1])
            if final_match is not None:
                self.final_bracket_round[0] = [final_match.score_1, final_match.score_2]

        if len(third_place) > 0:
            third_place = self.find_bracket_game(third_place[0], third_place[1])
            if third_place is not None:
                self.final_bracket_round[1] = [third_place.score_1, third_place.score_2]

        # Sort houses
        for house in self.houses.values():
            house.sort()


    def find_player(self, guid):
        for house in self.houses.values():
            for player in house:
                if player.guid == guid:
                    return player

        raise Exception("Nope")

    def find_bracket_game(self, player_1, player_2):
        # print('Searching for', player_1.guid.split('-')[0], player_2.split('-')[0])
        for game in self.bracket_games:
            # print('Game:', game.player_1.split('-')[0], game.player_2.split('-')[0])
            if game.player_1 == player_1 and game.player_2 == player_2:
                return game

            if game.player_2 == player_1 and game.player_1 == player_2:
                return game


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]
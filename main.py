from flask import render_template
from flask import Flask
from leaderboard import *
app = Flask(__name__)

@app.route('/')
def hello_world():
    return render_template("index.html", leaderboard=LeaderBoard("score.json"))
